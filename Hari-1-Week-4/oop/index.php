<?php
require('animal.php');
require('ape.php');
require('frog.php');


$sheep = new Animal("shaun");
$sunggokong = new Ape("Kera Sakti");
$kodok = new Frog("Buduk");

echo "Name: $sheep->name<br>legs: $sheep->legs<br>cold blooded: $sheep->cold_blooded<br><br>";
echo "Name: $kodok->name<br>legs: $kodok->legs<br>cold blooded: $kodok->cold_blooded<br>Jump: ";
echo $kodok->jump()."<br><br>";
echo "Name: $sunggokong->name<br>legs: $sunggokong->legs<br>cold blooded: $sunggokong->cold_blooded<br>Yell: ";
echo $sunggokong->yell()."<br><br>";
?>