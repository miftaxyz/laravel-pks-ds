<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Film;
use App\Models\Genre;

class BerandaController extends Controller
{
    public function index()
    {
        $film = Film::all();
        
        return view('beranda.index', compact('film'));
        
    }
}
