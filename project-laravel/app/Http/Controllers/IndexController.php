<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class IndexController extends Controller
{
    public function home()
    {
        return view('admin_form.index');
    }

    public function tabelData()
    {
        return view('admin_form.tabel_data');
    }

}
