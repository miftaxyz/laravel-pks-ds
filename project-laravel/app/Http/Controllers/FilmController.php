<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Film;
use App\Models\Genre;

class FilmController extends Controller
{
    public function create()
    {
        return view('film.create');
    }

    public function store(Request $request)
    {
        $this->validate($request,[
        'judul'=>'required',
        'tahun'=>'required',
        'ringkasan'=>'required',
        'nama'=>'required',
        'poster'=>'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048']);

        $poster = 'img-'.time().'.'.$request->poster->extension();  
       
        $request->poster->move(public_path('poster'), $poster);

        Film::create([
            'judul'=>$request->judul,
            'tahun'=>$request->tahun,
            'ringkasan'=>$request->ringkasan,
            'poster'=>$poster
        ]);

        Genre::create([
            'nama'=>$request->nama
        ]);

        return redirect('/');
    }

    public function show($id)
    {
        $film = Film::find($id);
        $genre = Genre::find($id);
        return view('film.show', compact('film','genre'));
    }

    public function edit($id)
    {
        $film = Film::find($id);
        return view('film.edit', compact('film'));
    }

    public function update($id, Request $request)
    {
        $request->validate([
            'judul'=>'required',
            'tahun'=>'required',
            'ringkasan'=>'required',
            'poster'=>'required',
        ]);

        $film = Film::find($id);
        $film->judul = $request->judul;
        $film->tahun = $request->tahun;
        $film->ringkasan = $request->ringkasan;

        

        if($poster = $request->file('poster')){
            $poster = 'img-'.time().'.'.$request->poster->extension();  
            $request->poster->move(public_path('poster'), $poster);
            $request->poster = $poster;
        }else{
            unset($request->poster);
        }

        $film->poster = $poster;


        $film->update();
        return redirect('/');
    }

    public function destroy($id)
    {
        $film = Film::find($id);
        $film->delete();
        return redirect('/');
    }

    
}
