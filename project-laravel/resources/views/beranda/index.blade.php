@extends('layout_admin.index');

@section('judul')
    <h2>Ringkasan FIlm</h2>
@endsection

@section('content')
<a href="/film-create" class="btn btn-primary mb-3">Tambah</a>
    

    @forelse ($film as $key=>$value)
        <div class="card" style="width: 18rem;">
            <img class="card-img-top" src="poster/{{$value->poster}}" alt="Card image cap">
            <div class="card-body">
            <h4 class="card-title">Judul: {{$value->judul}}</h4>
            </div>
            <ul class="list-group list-group-flush">
            <li class="list-group-item">Genre: {{$value->genre ? $value->genre->nama : '-'}}</li>
            <li class="list-group-item">Tahun: {{$value->tahun}}</li>
            <li class="list-group-item">Ringkasan: {{$value->ringkasan}}</li>
            </ul>
            <div class="card-body">
            <a href="/film/{{$value->id}}" class="btn btn-info">Show</a>
            <a href="/film/{{$value->id}}/edit" class="btn btn-primary">Edit</a>
            <form action="/film/{{$value->id}}" method="POST">
                @csrf
                @method('DELETE')
                <input type="submit" class="btn btn-danger my-1" onclick="return confirm('Yakin ingin Menghapus?')" value="Delete">
            </form>
            </div>
        </div>
    @empty
        <tr colspan="3">
            <td>No data</td>
        </tr>  
    @endforelse 
@endsection