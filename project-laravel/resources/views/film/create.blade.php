@extends('layout_admin.index');

@section('content')
<a href="{{url()->previous()}}" class="btn btn-secondary mb-3">Back</a>
    <h2>Tambah Data</h2>
    <form action="/" method="POST" enctype="multipart/form-data">
        @csrf
        <div class="form-group">
            <label for="judul">Judul</label>
            <input type="text" class="form-control" name="judul" id="judul" placeholder="Masukkan Judul">
            @error('judul')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <div class="form-group">
            <label for="tahun">Tahun</label>
            <input type="text" class="form-control" name="tahun" id="tahun" placeholder="Masukkan Tahun">
            @error('tahun')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <div class="form-group">
            <label for="nama">Nama Genre</label>
            <input type="text" class="form-control" name="nama" id="nama" placeholder="Masukkan Genre">
            @error('nama')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <div class="form-group">
            <label for="ringkasan">Ringkasan</label>
            <textarea class="form-control" name="ringkasan" id="ringkasan" cols="30" rows="10" placeholder="Masukkan Bio"></textarea>
            @error('ringkasan')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>

        <div class="form-group">
            <label class="form-label" for="poster">Select Image:</label>
            <input 
                type="file" 
                name="poster" 
                id="poster"
                class="form-control @error('poster') is-invalid @enderror">

            @error('poster')
                <span class="text-danger">{{ $message }}</span>
            @enderror
        </div>
        <button type="submit" class="btn btn-primary">Tambah</button>
    </form>
@endsection