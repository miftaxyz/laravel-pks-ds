@extends('layout_admin.index');

@section('content')
    <h2>Daftar Film</h2>
    <img class="card-img-top" src="{{asset('poster/'.$film->poster)}}" alt="Card image cap">
    <h4>Judul: {{$film->judul}}</h4>
    <p>Tahun: {{$film->tahun}}</p>
    <p>Genre: {{$film->genre->nama}}</p>
    <p>Ringkasan: {{$film->ringkasan}}</p>
    <a href="{{url()->previous()}}" class="btn btn-secondary mb-3">Back</a>
@endsection