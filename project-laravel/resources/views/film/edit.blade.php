@extends('layout_admin.index');

@section('content')
<a href="{{url()->previous()}}" class="btn btn-secondary mb-3">Back</a>
    <h2>Edit Data</h2>
    <form action="/film/{{$film->id}}" method="POST" enctype="multipart/form-data">
        @csrf
        @method('PUT')
        <div class="form-group">
            <label for="judul">Judul</label>
            <input type="text" class="form-control" name="judul" id="judul" value="{{$film->judul}}" placeholder="Masukkan Judul">
            @error('judul')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <div class="form-group">
            <label for="tahun">Umur</label>
            <input type="text" class="form-control" name="tahun" id="tahun" value="{{$film->tahun}}" placeholder="Masukkan Tahun">
            @error('tahun')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <div class="form-group">
            <label for="ringkasan">Ringkasan</label>
            <textarea class="form-control" name="ringkasan" id="ringkasan" cols="30" rows="10" placeholder="Masukkan Ringkasan">{{$film->ringkasan}}</textarea>
            @error('ringkasan')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <div class="form-group">
            <label class="form-label" for="poster">Select Image:</label>
            <input 
                type="file" 
                name="poster" 
                id="poster" 
                class="form-control @error('poster') is-invalid @enderror">

            @error('poster')
                <span class="text-danger">{{ $message }}</span>
            @enderror
        </div>
        <button type="submit" class="btn btn-primary">Edit</button>
    </form>
@endsection