@extends('layout_admin.index')

@section('content')
    <h1>SELAMAT DATANG!  <span style="text-transform: capitalize;">{{$firstName}} {{$lastName}}</span> </h1>
    <h4>Terima kasih telah bergabung di Website Kami. Media Belajar kita bersama!</h4>
@endsection

@section('judul')
    Welcome
@endsection