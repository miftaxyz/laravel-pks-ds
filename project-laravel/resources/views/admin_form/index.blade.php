@extends('layout_admin.index')

@section('content')
    <h1>Media Online</h1>
    <h3>Sosial Media Developer</h3>
    <p>belajar dan berbagi agar hidup menjadi lebih baik</p>
    <h3>Benefit Join di Media Online</h3>
    <ul>
        <li>Mendapatkan motivasi dari sesama para Developer</li>
        <li>Sharing knowledge</li>
        <li>Dibuat oleh calon web developer terbaik</li>
    </ul>
    <h3>Cara Bergabung ke Media Online</h3>
    <ol>
        <li>Mengunjungi Website ini</li>
        <li>Mendaftarkan di <a href="{{route('register')}}">Form Sign Up</a></li>
        <li>Selesai</li>
    </ol>
@endsection

@section('judul')
    Halaman Form
@endsection