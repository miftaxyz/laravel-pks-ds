@extends('layout_admin.index')

@section('content')
    <h1>Buat Account Baru</h1>
    <h3>Sign Up Form</h3>
    <form action="/welcome" method="POST">
        @csrf
        <label>First name:</label><br>
        <input type="text" name="firstName"><br><br>
        <label>Last name:</label><br>
        <input type="text" name="lastName"><br><br>
        <label>Gender</label><br>
        <input type="radio" name="gender" value="Male">Male<br>
        <input type="radio" name="gender" value="Female">Female<br><br>
        <label>Nationality</label><br>
        <select name="nationality">
            <option value="Indonesia" selected>Indonesia</option>
            <option value="Amerika">Amerika</option>
            <option value="Inggris">Inggris</option>
        </select><br><br>
        <label>Language Spoken</label><br>
        <input type="checkbox" name="language" value="Bahasa Indonesia">Bahasa Indonesia<br>
        <input type="checkbox" name="language" value="English">English<br>
        <input type="checkbox" name="language" value="Other">Other<br><br>
        <label>Bio</label><br>
        <textarea name="Bio" rows="10" cols="30"></textarea><br><br>
        <input type="submit">
    </form>

@endsection

@section('judul')
    Isian Form
@endsection